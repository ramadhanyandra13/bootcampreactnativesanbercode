//soal1
/*
    Tulis code function promise di sini
*/
const myCountPromise=(param) =>{
    return new Promise((resolve,reject)=>{
        if(param===2){
            setTimeout(()=>{
                resolve(2*2)
            },2000)
           
        }else {
            reject("Maaf tidak ada nilai dalam parameter")
        }
    })
}

//kode dibawah ini jangan dihapus atau diedit sama sekali ya


myCountPromise(2)
 .then((result) => {
    console.log(result)
 })
.catch((error) => {
    console.log(error)
})
