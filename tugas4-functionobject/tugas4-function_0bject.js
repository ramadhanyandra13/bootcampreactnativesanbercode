//soal1

function cetakFunction(ramadhan) {
    console.log("Halo Nama saya"+ ramadhan)
}

console.log(cetakFunction("ramadhan"))

//soal2

function myFunction(){
    return(angka1+angka2)
 }
 
 let angka1 = 20
 let angka2 = 7
 let output = myFunction(angka1, angka2)
 
 console.log(output)

 //soal3
 const Hello = ()=>{

    return "Hello"
    
    }

    Hello()

//soal 4
let obj = {
    nama : "john",
    umur : 22,
    bahasa : "indonesia"
    }

console.log(obj.bahasa)

//soal5
let arrayDaftarPeserta = ["Ramadhan Yandra Lubis", "laki-laki", "baca buku" , 1999]
let objDaftarPeserta = {
    nama : arrayDaftarPeserta[0],
    jenisKelamin : arrayDaftarPeserta[1],
    hobby : arrayDaftarPeserta[2],
    tahunLahir : arrayDaftarPeserta[3],
}

console.log(objDaftarPeserta)

//SOAL6
var objDaftarBuah= [
    {nama :"Nanas", warna :"Kuning",adaBijinya:"tidak",harga:9000 },
    {nama :"Jeruk", warna :"Oranye",adaBijinya:"ada",harga:8000},
    {nama :"Semangka", warna :"Hijau&Merah",adaBijinya:"ada",harga:10000},
    {nama :"Pisang", warna :"Kuning",adaBijinya:"tidak",harga:5000}]

console.log(objDaftarBuah)

let filterBiji = objDaftarBuah.filter(function(result){
    return result.adaBijinya === "tidak"
})

console.log(filterBiji)

//soal7
let phone = {
    name: "Galaxy Note 20",
    brand: "Samsung",
    year: 2020
 }
 // kode diatas ini jangan di rubah atau di hapus sama sekali
 /* 
 Tulis kode jawaban destructuring di sini 
 */
 const {name,brand,year}=phone

 
 // kode di bawah ini jangan dirubah atau dihapus
 console.log(name, brand, year) 

 //soal8
 let dataBukuTambahan= {
    penulis: "john doe",
    tahunTerbit: 2020 
  } 
  let buku = {
    nama: "pemograman dasar",
    jumlahHalaman: 172
  }
  let objOutput = {   
  } 
  // kode diatas ini jangan di rubah atau di hapus sama sekali 
  /* 
      Tulis kode jawabannya di sini 
  */ 
  console.log({...dataBukuTambahan,...buku})
  // kode di bawah ini jangan dirubah atau dihapus
  console.log(objOutput) 

  //soal9
  let mobil = {
    merk : "bmw",
    color: "red",  
    year : 2002
    }
     
    const functionObject = (param) => {

        return param
        }

   console.log(functionObject(mobil))
   
