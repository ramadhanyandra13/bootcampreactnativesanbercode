//soal1
for (let a = 0; a<10; a++) {
    console.log(a);
   }
//soal2
for(let a=1 ; a<10; a++){
    if(a%2==1){
        console.log(a);
    }
}
//soal3
for(let b=0; b<10; b++){
    if(b%2==0){
        console.log(b);
    }
}
//soal4
let array1 = [1,2,3,4,5,6]
console.log(array1[5])

//soal5
let array2 = [5,2,4,1,3,5]
 array2.sort();
console.log(array2)

//soal6
let array3 = ["selamat", "anda", "melakukan", "perulangan", "array", "dengan", "for"]

for (let i = 0; i<7; i++) {
    console.log (array3[i]);
    }

//soal7
let array4 = [1, 2, 3, 4, 5, 6,7, 8, 9, 10]

for (let i = 0; i<=10; i++) {
    if (i%2==1) {
       console.log(array4[i])
        
    }
}

//soal8
let kalimat= ["saya", "sangat", "senang", "belajar", "javascript"]
let pisah=kalimat.join(" ");
console.log(pisah)

//soal9
var sayuran=[];
sayuran[0] = 'kangkung'
sayuran[1] = 'bayam'
sayuran[2] = 'buncis'
sayuran[3] = 'kubis'
sayuran[4] = 'timun'
sayuran[5] = 'seledri'

sayuran.push('toge')
console.log(sayuran)
